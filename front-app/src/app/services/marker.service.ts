import { Injectable } from "@angular/core";
import { marker } from "leaflet";
// import { API_URL } from "../constants/api";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { catchError } from 'rxjs/operators';
import { Marker } from "../models/marker";

@Injectable({
  providedIn: "root"
})
export class MarkerService {
  url: string = "http://localhost:10036/marker";

  private markers = [
    marker([ 46.879966, -121.726909 ]).bindPopup("I am a marker from a service"),
    marker([4.702033521865703, -74.14702552362245]).bindTooltip("tooltip!").bindPopup("popup")
  ];

  constructor(
    private http: HttpClient
  ) { }

  getMarkers(): Observable<Marker[]> {
    return this.http.get<Marker[]>(this.url)
      .pipe(
        catchError(this.handleError<Marker[]>("Get markers"))
      );
  }

  addMarker(marker: Marker): Observable<any> {
    return this.http.post<any>(this.url, marker)
      .pipe(
        catchError(this.handleError<any>("Add marker"))
      );
  }

  deleteMarker(id: number): Observable<any> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url)
      .pipe(
        catchError(this.handleError<any>("Delete marker"))
      )
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }
}
