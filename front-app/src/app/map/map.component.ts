import { Component, OnInit, Input } from "@angular/core";
import { latLng as latLngMaker, tileLayer, circle, polygon, marker, layerGroup } from "leaflet";
import { MarkerService } from "../services/marker.service";
import { Marker } from "../models/marker";
import { X, SAVE } from "../constants/modal";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgElement, WithProperties } from '@angular/elements';

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"]
})
export class MapComponent implements OnInit {
  options: object = {
    layers: [
      tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 18, attribution: "..." })
    ],
    zoom: 5,
    center: latLngMaker(4.624335, -74.063644),
    doubleClickZoom: false,
  };

  layersControl = {
    baseLayers: {
      "Open Street Map": tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 18, attribution: "..." }),
      "Open Cycle Map": tileLayer("http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png", { maxZoom: 18, attribution: "..." })
    },
    overlays: {
      "Big Circle": circle([ 46.95, -122 ], { radius: 5000 }),
      "Big Square": polygon([[ 46.8, -121.55 ], [ 46.9, -121.55 ], [ 46.9, -121.7 ], [ 46.8, -121.7 ]])
    },
  }

  layers: Array<any> = [];
  events: Object = { X, SAVE }
  @Input() marker: Marker = new Marker();
  loading: boolean = false;

  constructor(
    private markerService: MarkerService,
    private modalService: NgbModal,
  ) { 
  }

  ngOnInit() {
    this.getMarkers();
  }

  getMarkers() {
    this.markerService.getMarkers()
    .subscribe(markers => {
      markers.forEach(mrk => {
        this.addMarker(mrk);
      });
    });
  }

  addMarker(mrk: Marker): void {
    let newMarker = marker([mrk.lat,mrk.lng])
    if(mrk.tooltip) {
      newMarker = newMarker.bindTooltip(mrk.tooltip);
    }
    if(mrk.popup) {
      newMarker = newMarker.bindPopup((fl) => this.createPopupComponent(mrk));
    }
    this.layers.push(newMarker);
  }

  
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: "modal-basic-title"})
    .result.then((result) => {
      this.handleClose(result);
    }, (reason) => {
      console.log(reason);
    });
  }


  onMapDoubleClick(content, evt) {
    const { lat, lng } = evt.latlng;
    this.open(content);
    this.marker.lat = lat;
    this.marker.lng = lng;
  }

  
  createMarker(): void {
    this.markerService.addMarker(this.marker)
      .subscribe(res => {
        if(res.status === 200) {
          this.addMarker(res.body);
        }

        this.marker = new Marker();
      });
  }

  
  private handleClose(reason: any): void {
    switch(reason) {
      case SAVE: {
        this.createMarker();
        return;
      }
      case X: {
        console.log("closing with x")
        return;
      }
    }
  }

  public createPopupComponent(mark: Marker) {
    const popupEl: NgElement & WithProperties<any> = document.createElement('div') as any;
    popupEl.innerText = mark.tooltip;
    const button = document.createElement("button");
    button.addEventListener("click", () => this.deleteMarker(mark.id));
    button.innerText = "Delete popup";
    button.style.marginLeft = "5px";
    popupEl.append(button);
    popupEl.addEventListener('closed', () => document.body.removeChild(popupEl));
    document.body.appendChild(popupEl);
    return popupEl;
  }

  private deleteMarker(id: any): void {
    this.markerService.deleteMarker(id)
      .subscribe(res => {
        const deleted = res.body.deleted;
        if(deleted) {
          const marker = res.body.marker;
          const { lng, lat } = marker;
          this.layers = this.layers.filter(layer => {
            const latLng = layer._latlng;
            return latLng.lat != lat && latLng.lng != lng;
          });
        }
      });
  }

  onTest(evt) {
    console.log(evt);
  }
}
