export class Marker {
  id: number;
  lat: number;
  lng: number;
  popup: string;
  tooltip: string;
  createdAt: Date;
  updatedAt: Date;
}