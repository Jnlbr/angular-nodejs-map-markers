require('dotenv/config');
const cors = require('cors');
// const morgan = require('morgan');
const http = require('http');
const express = require('express');
const { sequelize } =  require('./src/models');

const app = express();

app.use(cors());
app.use(express.json())


app.use('/', require('./src/routes'));

const httpServer = http.createServer(app);

const isTest = !!process.env.TEST_DATABASE;
const isProduction = !!process.env.DATABASE_URL;
const port = process.env.PORT || 8000;

sequelize.sync({ force: isTest || isProduction }).then(async () => {

  httpServer.listen({ port }, () => {
    console.log(`Server listening on http://localhost:${port}`);
  });
});