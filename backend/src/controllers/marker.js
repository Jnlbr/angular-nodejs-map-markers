const Marker = require("../models/marker").Marker;

async function add(req,res) {
  const send = (status,body) => res.status(status).send({ status, body });
  const marker = req.body;
  try {
    const newMarker = await Marker.create(marker);
    send(200, newMarker);
  } catch(err) {
    send(400, err.message || err);
  }
}

async function findAll(req,res) {
  const send = (status,body) => res.status(status).send({ status, body });
  try {
    const markers = await Marker.findAll();
    res.status(200).send(markers);
  } catch(err) {
    send(400, err.message || err);
  }
}

async function deleteOne(req,res) {
  const send = (status,body) => res.status(status).send({ status, body });
  const id = req.params.id;

  try {
    const marker = await Marker.findOne({ where: { id: id }});
    const deleted = await Marker.destroy({ where: { id }});
    send(200, {
      id, 
      deleted: deleted === 1? true: false,
      marker
    });
  } catch(err) {
    send(400, err.message || err);
  }
}

module.exports = {
  add,
  findAll,
  deleteOne,
  // update
}