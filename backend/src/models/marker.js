const { Model } = require("sequelize");

class Marker extends Model {};

function marker(sequelize, dataTypes) {
  Marker.init({
    lng: {
      type: dataTypes.REAL(16,16),
      allowNull: false
    },
    lat: {
      type: dataTypes.REAL(16,16),
      allowNull: false
    },
    popup: {
      type: dataTypes.STRING,
      allowNull: true,
      validate: {
        len: [1,64]
      }
    },
    tooltip: {
      type: dataTypes.STRING,
      allowNull: true,
      validate: {
        len: [1,32]
      }
    }
  }, {
    sequelize,
    modelName: "Marker",
    tableName: "marker",
    underscored: true,
    defaultScope: {
      attributes: {
        exclude: ["updated_at"]
      }
    }
  });

  return Marker;
}

module.exports = marker;
module.exports.Marker = Marker;