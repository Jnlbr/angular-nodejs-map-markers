const express = require("express");
const controllers = require("../controllers/marker");

const router = express.Router();


router.post("/", controllers.add);
router.get("/", controllers.findAll);
// router.put("/", controllers.update);
router.delete("/:id", controllers.deleteOne)


module.exports = router;